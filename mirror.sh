#!/bin/bash
#HOST="your.ftp.host.dom"
#USER="username"
#PASS="password"
#FTPURL="ftp://$USER:$PASS@$HOST"
#LCD="/path/of/your/local/dir"
#RCD="/path/of/your/remote/dir"

HOST=$1
USER=$2
PASS=$3
FTPURL="ftp://$USER:$PASS@$HOST"
RCD=$4
LCD=$5
#DELETE="--delete"
lftp -c "set ftp:list-options -a;
set ssl:verify-certificate no;
open '$FTPURL';
lcd $LCD;
cd $RCD;
mirror --reverse \
       --only-newer \
       --ignore-time \
       $DELETE \
       --verbose \
       --exclude-glob user/themes/bindungistbunt/node_modules/**/* \
       --exclude-glob user/themes/bindungistbunt/**/*.styl \
       --exclude-glob {.*/,backup/,cache/,logs/,tmp/,webserver-configs/}**/*"
