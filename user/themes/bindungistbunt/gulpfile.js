// include the required packages.
var gulp = require('gulp');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();
var autoprefixer = require('autoprefixer-stylus');
var mqpacker = require('css-mqpacker');

// include, if you want to work with sourcemaps
var sourcemaps = require('gulp-sourcemaps');

// Inline sourcemaps
gulp.task('styles', function () {
  return gulp.src('./templates/**/*.styl')
    .pipe(sourcemaps.init())
    .pipe(stylus({
      use: [autoprefixer()], // see browserslist file for support matrix
      paths: ['lib/stylus'],
      import: ['variables', 'breakpoints'], //, 'mediaQueries', 'mixins', 'suchen-mixins', 'extendables', 'gridHelper'],
      url: {
        name: 'embedurl',
      },
    }))
    .pipe(concat('bindungistbunt.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css/'));
});

gulp.task('styles:vendor', function() {
  return gulp.src('./node_modules/purecss/build/{base,grids,grids-responsive}-min.css')
  .pipe(concat('bindungistbunt.vendor.min.css'))
  .pipe(gulp.dest('./css/'));
});

gulp.task('dev', ['styles', 'styles:vendor'],function() {
  browserSync.init({
    proxy: {
      target: 'localhost'
    },
    serveStatic: ["css"],
    files: "css/bindungistbunt.css"
  });
   gulp.watch("./templates/**/*.styl", ['styles']);
   gulp.watch("templates/**/*.html.twig").on('change', browserSync.reload);
});

gulp.task('images', () =>
    gulp.src('../../pages/**/*.{jpg,jpeg,png}')
        .pipe(imagemin({
          interlaced: true,
           progressive: true,
           optimizationLevel: 5,
           svgoPlugins: [{removeViewBox: true}]
        }))
        .pipe(gulp.dest('../../pages/'))
);

// Default gulp task to run
gulp.task('default', ['styles', 'styles:vendor']);
