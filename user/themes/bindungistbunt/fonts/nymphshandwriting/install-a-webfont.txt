Installing Webfonts

1. Upload the files from this zip to your domain.
2. Add this code to your website:

@font-face {
    font-family: 'Nymphs-Handwriting';
    src:url('Nymphs-Handwriting.ttf.woff') format('woff'),
        url('Nymphs-Handwriting.ttf.svg#Nymphs-Handwriting') format('svg'),
        url('Nymphs-Handwriting.ttf.eot'),
        url('Nymphs-Handwriting.ttf.eot?#iefix') format('embedded-opentype'); 
    font-weight: normal;
    font-style: normal;
}
3. Integrate the fonts into your CSS:
Add the font name to your CSS styles. For example:

h1 { 
   font-family: 'Nymphs-Handwriting';
}  

Troubleshooting Webfonts
1. You may be using the fonts on different domain or subdomain.
2. Check if you have link the fonts properly in the CSS.