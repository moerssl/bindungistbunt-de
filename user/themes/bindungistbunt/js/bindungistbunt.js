var body = document.getElementsByTagName('body')[0];
var headerPlayIcon = document.querySelector('[data-video-play]');
var headerReallyPlayIcon = document.querySelector('[data-video-play-really]');
var headerVideo = document.querySelector('[data-video]');

body.addEventListener('tracker::loaded', function (e) {
  var elementsToTrack = document.querySelectorAll('[data-track]');

  elementsToTrack.forEach(function(item, index){
    item.onclick = function(e) {
      var payload =   JSON.parse(this.getAttribute('data-track'));
      _paq.push(['trackEvent', payload.category, payload.action, payload.name, payload.value])
      }
  });

 }, false);
 
 body.addEventListener('facebook::enable', function (e) {
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '310464009561764',
      xfbml      : true,
      version    : 'v2.7'
    });

    // Get Embedded Video Player API Instance
    var my_video_player;
    
      // Get Embedded Video Player API Instance
      FB.Event.subscribe('xfbml.ready', function(msg) {
        if (msg.type === 'video') {
          msg.instance.unmute();
        }
      });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v3.2';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
 }, false);

 headerPlayIcon.onclick = function() {
   console.log('enable faceboojk, play video');
  var event = new Event('facebook::enable');
  body.dispatchEvent(event);
  headerVideo.classList.add('video__video--show');
 }

 headerReallyPlayIcon.onclick = function() {
  headerReallyPlayIcon.classList.add('icon-hidden')
}
