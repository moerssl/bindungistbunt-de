---
title: Mitmachen
metadata:
  description: Jeder der sich mit unseren Werten identifiziert und das zeigen möchte, darf unser Logo verwenden, unseren Text auf seine Website stellen und teilen. Wir stellen das Logo und den Text kostenfrei zur Verfügung.
---
<a class="img center" href="https://die-besten-stoffwindeln.de/spenden/button-bindung-ist-bunt" target="_blank" data-track='{"category": "mitmachen-link", "action": "stoffwindeln", "name": "bild"  }'>
![Unsere Buttons](bindung-ist-bunt-button.jpg)
</a>
<section class="navigation-secondary__wrapper">
  <div class="navigation-secondary wrapper pure-u-lg-18-24">
    <div class="navigation-secondary__item">
      <a class="navigation-secondary__link" href="https://die-besten-stoffwindeln.de/spenden/button-bindung-ist-bunt" target="_blank" data-track='{"category": "mitmachen-link", "action": "stoffwindeln", "name": "stoffwindeln"  }'>Jetzt Buttons bestellen</a>
    </div>  
  </div>
</section>

# Einfach mitmachen!

Folgt uns auf Facebook ([Nora](https://www.facebook.com/Imlau.Nora/) | [Nicola](https://www.facebook.com/artgerechtprojekt)), Instagram ( [Nora](https://www.instagram.com/noraimlau/) | [Nicola](https://www.instagram.com/artgerechtnic)) und Twitter ([Nora](https://twitter.com/planet_eltern) | [Nicola](https://twitter.com/artgerecht)) und bleibt auf dem Laufenden, wie sich das Projekt entwickelt.

Jeder der sich mit unseren Werten identifiziert und das zeigen möchte, ist herzlich eingeladen, unser Logo zu verwenden und unseren Text auf seine Website zu stellen und zu teilen. Wir stellen euch unser Logo und den Text kostenfrei zur Verfügung, bitte denkt an den Quellenverweis (www.bindungsistbunt.de).

Du willst mit anderen über das Thema ins Gespräch kommen? Hier findest du unseren Button zum Tragen und Verschenken.

Du bloggst, hast ein Unternehmen oder bietest eine Dienstleistung an und möchtest uns offiziell unterstützen?
Dann melde Dich bei uns unter kontakt@bindungistbunt.de! Es ist möglich, Dich mit Deinem Logo auf unserer Unterstützer-Seite einzutragen.

Wir behalten uns vor, alle Anträge auf Übereinstimmung mit unseren Werten zu prüfen.



## Bindung ist bunt ist kein Verein

Bindung ist bunt - das sind bislang nur wir, Nora und Nicola. Wir sind kein Verein, keine große Organisation, kein Büro, kein Team. Damit Bindung ist bunt groß werden kann, braucht es Menschen, die sich mit unseren Werten identifizieren.
Gehörst Du dazu, schließ dich uns an und trag unsere Botschaft in Deine Welt: Verteile unsere Buttons, stell unsere Logo auf deine Website, verlinke zu unserer Seite, trage unser Zeichen an deiner Jacke und lass es uns wissen, wenn du dadurch mit anderen Menschen ins Gespräch kommst über Bindung und Beziehung und Familie und uns und Dich.


