---
title: Warum diese Initiative?
metadata:
  description: Bindungsorientierte Elternschaft wird vielfach als Lebenshaltung verstanden, die weit über die ersten Lebensjahre eines Kindes hinaus reicht und die nahezu jeden Aspekt des Familienlebens berührt.

---
# Bindung ist bunt!

## Eine Initiative von Nora Imlau und Nicola Schmidt

### Warum diese Initiative?

Bindungsorientierte Ansätze werden in der deutschsprachigen Öffentlichkeit immer wieder scharf kritisiert: Als hochideologisch und weltfremd, als unemanzipiert und frauenfeindlich, als unwissenschaftlich und esoterisch, als gnadenloser Supermutterwettbewerb.

Ein Grund dafür: Bindungsorientierte Elternschaft ist in Deutschland, Österreich und der Schweiz auf dem Vormarsch. Mehr und mehr Eltern setzen auf Bindung und Beziehung als die Grundlage ihres Familienlebens. Doch das Verständnis darüber, was bindungsorientierte Elternschaft ganz konkret bedeutet, geht teilweise weit auseinander.

Ein Anhaltspunkt zur Begriffsklärung kann die Definition des „Attachment Parenting“ nach Dr. William Sears sein, das der bindungsorientierten Bewegung in Deutschland vielfach als Blaupause diente. Sears definiert sein „Attachment Parenting“ als eine Haltung der liebevollen Zugewandtheit, der Feinfühligkeit und der Responsivität, die das Entstehen einer sicheren Bindung in der Baby- und Kleinkindzeit befördert.

Im deutschsprachigen Raum hat der Begriff in den vergangenen Jahren jedoch einen immensen Bedeutungswandel erfahren: Bindungsorientierte Elternschaft wird vielfach als Lebenshaltung verstanden, die weit über die ersten Lebensjahre eines Kindes hinaus reicht und die nahezu jeden Aspekt des Familienlebens berührt – von der Ernährung über die Wahl des Autositzes bis hin zu Fragen von Betreuung und Bildung.

Als Autorinnen machen wir uns seit vielen Jahren öffentlich für bindungsorientierte Elternschaft stark. Deshalb ist es uns wichtig, zu erklären, was bindungsorientierte Elternschaft für uns bedeutet und für welche Grundwerte wir einstehen, wenn wir sie vertreten.
