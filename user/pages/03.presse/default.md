---
title: Presse
metadata:
  description: Sie haben eine Frage zu der Initiative oder zu uns? Sie möchten uns als Interviewpartner oder als Referenten anfragen? Wir freuen uns darauf, von Ihnen zu lesen.
---
# Bindung ist bunt
## Presseanfrage
Sie haben eine Frage zu der Initiative oder zu uns?
Sie möchten uns als Interviewpartner oder als Referenten anfragen?
Wir freuen uns darauf, von Ihnen zu lesen.

<section class="video__container" data-video-play>
  <div class="video__icon video__icon--play" >
  <span class="icon">&#x25BA</span>
  <span class="text">Video ansehen</span>
  </div>

  <img src="/user/images/facebook-live-1.png" />
  <div class="video__video" data-video>
    <div class="fb-video" data-href="https://www.facebook.com/artgerechtprojekt/videos/1158060391030389/"
      data-allowfullscreen="false"
      data-autoplay="false" data-show-text="false">
    </div>
  </div>
</section>