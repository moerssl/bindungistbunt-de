---
title: Bindung ist bunt!
titleelement: h1
content:
  items: @self.modular
---
<section class="video__container" data-video-play>
  <div class="video__icon video__icon--play" >
  <span class="icon">&#x25BA</span>
  <span class="text">Video ansehen</span>
  </div>

  <img src="/user/images/facebook-live-1.png" />
  <div class="video__video" data-video>
    <div class="fb-video" data-href="https://www.facebook.com/artgerechtprojekt/videos/1158060391030389/"
      data-allowfullscreen="false"
      data-autoplay="false" data-show-text="false">
    </div>
  </div>
</section>


Bindungsorientiert zu leben, das bedeutet für uns vor allem eins: Wertschätzung. Diese Haltung in die Welt zu tragen, ist uns seit über zehn Jahren ein echtes Herzensanliegen.
Deshalb gibt es jetzt „Bindung ist bunt!“ – eine Initiative für sichtbare Solidarität unter Eltern.

„Bindung ist bunt!“ - das heißt für uns: Liebevolles Familienleben kann ganz verschieden aussehen. Auf die Beziehung kommt es an!  

„Bindung ist bunt!“, das heißt aber auch: Für echte Vielfalt in unserem Land einzustehen und uns stark zu machen gegen jede Form von Hass, Diskrimierung und Gewalt.
Mit unseren Buttons wollen wir euch die Möglichkeit geben, sichtbar zu werden und einander zu finden – als Eltern, die für bindungsorientiertes Familienleben, Toleranz und Respekt einstehen.
