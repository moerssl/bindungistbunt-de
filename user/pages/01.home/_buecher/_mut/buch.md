---
title: 'Mut - Wie Kinder über sich hinauswachsen'
media_order: nicola-schmidt-mut-bindung-ist-bunt.jpg
buch7: https://www.buch7.de/store/product_details/1022442151
amazon: https://amzn.to/2KFCaJu
---
Warum wollen Kleinkinder immer höher schaukeln? Warum spielen sie im größten Dreck? Warum hören Eltern 100x am Tag »kann ich selber!« – bis es ans Zubettgehen geht?

Nicola Schmidt erklärt, warum Kinder die Gefahr suchen und brauchen – und wie dadurch Mut entsteht. Es erzählt davon, wie wir Kinder ermutigen können, wie sie schwierige Situationen meistern und welche Mutproben wichtig sind. Das Buch zeigt Eltern, wie sie den Mut ihres Kindes als Muskel begreifen und fördern können – und wie sie selbst mutiger werden und gelassener mit eigenem Scheitern umgehen können. Empfohlen vom Gewaltpräventionsprojekt MUT TUT GUT - NRW.
