---
title: 'Slow Family'
media_order: nicola-schmidt-slow-family-bindung-ist-bunt.jpg
buch7: https://www.buch7.de/store/product_details/1026636787
amazon: https://amzn.to/2MGG7ei
---
** Sieben Zutaten f&uuml;r ein einfaches Leben mit Kindern **

Überall, wo Kinder in die Welt aufbrechen, gibt es Alternativen zu einem Leben, das immer schneller, technischer und komplizierter wird. In diesem Buch zeigen Julia Dibbern und Nicola Schmidt, wie Eltern und Kinder ihre Bedürfnisse nach Nähe, Natur und Langsamkeit gemeinsam ausleben können.

Die beiden Pionierinnen der Artgerecht-Bewegung haben Wege zu mehr Entschleunigung und Nachhaltigkeit im Alltag mit Kindern gefunden, und zwar jenseits vom Vereinbarkeitsstress isolierter Kleinfamilien. Denn Eltern, die sich gemeinschaftlich organisieren, finden nicht nur Entspannung und Abwechslung, sondern auch Lösungen für ein ökonomisches System, das genauso unter Druck steht wie die Mütter und Väter von heute.
