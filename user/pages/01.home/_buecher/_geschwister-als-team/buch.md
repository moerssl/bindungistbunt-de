---
title: 'Geschwister als Team'
media_order: nicola-schmidt-geschwister-als-team-bindung-ist-bunt.jpg
buch7: https://www.buch7.de/store/product_details/1033558538
amazon: https://amzn.to/2JqENim
---
**Endlich Frieden in der Familie**

»Ein Kind ist kein Kind – und zwei sind vier!« Mit jedem Geschwisterchen steigt der Trubelfaktor in einer Familie exponentiell. Warum? Aus Sicht der Evolution sind Geschwister Rivalen, die um Nahrung und Sicherheit konkurrieren. »Ich will zuerst Apfelsaft! Nein, das ist mein Platz! Ich will vorne sitzen!«

Wenn Eltern dieses Buch gelesen haben, wissen sie, worum die Kinder wirklich streiten. Und auch, wie sie am besten reagieren, um sie beim Zusammenwachsen zu unterstützen. Nicola Schmidt zeigt genial einfache Wege, schlimmste Rivalen zu starken Teams werden zu lassen. So wird es leicht, konstruktiv zu reagieren, wenn alle Kinder gleichzeitig »Ich zuerst!« schreien.
