---
title: Artgerecht - Das andere Kleinkindbuch
media_order: nicola-schmidt-artgerecht-das-andere-kleinkindbuch-bindung-ist-bunt.jpg
buch7: https://www.buch7.de/store/product_details/1031403452
amazon: https://amzn.to/2JzgCKD
---
Tausende begeisterte Eltern lesen alles, was Nicola Schmidt zur »artgerechten« Kindererziehung schreibt. Viele von ihnen warten sehnsüchtig auf die Fortsetzung des erfolgreichen Babybuchs.

Was passiert im Nervensystem, im rasant wachsenden Körper, während der Hormongewitter, wenn aus Babys Kleinkinder werden? Was hat die Evolution ihnen an Entwicklungsaufgaben mitgegeben und warum bringt das Eltern manchmal zur Verzweiflung? Wo hilft im Alltag die Wissenschaft weiter und wo wirken immer noch Ammenmärchen in unseren Köpfen?
