---
title: 'Babybauchzeit'
media_order: 'nora-imlau-babybauchzeit-bindung-ist-bunt'
buch7: https://www.buch7.de/store/product_details/1033586043
amazon: https://amzn.to/2MDpr7i
---
** Geborgen durch die Schwangerschaft und die Zeit danach. Hebammenwissen für Mutter und Kind **

Einfühlsam, optimistisch und nah an den Emotionen der werdenden Mutter begleitet dieses Buch die Wandlungen und Fragen von Schwangeren. Die erfahrene Hebamme Sabine Pfützner hat auch die Schwangerschaften von Nora Imlau begleitet. Gemeinsam mit der Eltern-Expertin und dreifachen Mutter stärkt sie das Vertrauen in den eigenen Körper und ermutigt, die Babybauchzeit als Feier des Lebens zu genießen.
Auf der Grundlage neuster Erkenntnisse aus Schulmedizin und Naturheilkunde vermitteln die beiden Autorinnen alles, was für Wohlbefinden, Gesundheit und Entspannung von Mutter und Kind wichtig ist. Auf Augenhöhe gehen sie auf weit verbreitete Ängste, seelische Dynamiken und Veränderungen im Alltag ein. Ein Buch wie eine starke Hebamme und eine gute Freundin, die zuhören kann, ehrlich ist, und Gelassenheit schenkt.
