---
title: 'Erziehen ohne Schimpfen'
media_order: 'erziehen-ohne-schimpfen-bindung-ist-bunt.jpg'
image: 'erziehen-ohne-schimpfen-bindung-ist-bunt.jpg'
buch7: 'https://www.buch7.de/store/product_details/1037397184'
amazon: 'https://amzn.to/2NjgBPM'
---

Schimpfen ist in vielen Familien ein fester Bestandteil der Erziehung. Wenn das Kind etwas angestellt hat oder nicht hören will, wird geschimpft. Eltern ertappen sich dabei, dass sie sich nicht anders zu helfen wissen. Aber kann Schimpfen auch schaden? Und geht es überhaupt ohne? Tatsächlich kann Schimpfen unerwünschte Nebenwirkungen haben: Das Selbstwertgefühl der Kinder leidet, der Lerneffekt ist selten positiv und es belastet die Beziehung; die Eltern plagt danach oft ein Gefühl der Scham. Nicola Schmidt zeigt die Alternativen zum Schimpfen. Ein wichtiger Schritt ist es, die Ursachen zu erkennen. Durch Organisation des Familienalltags, simple Minuten-Übungen und Schulung der Achtsamkeit kommt man oft schon ein großes Stück voran. Auch um Kommunikation geht es, um Regeln und darum, wie man mit Fehlern umgeht: Mit Klarheit, Empathie und Spiegelung lassen sich Konflikte viel besser lösen als mit Lautstärke. Eine 21-Tage-Challenge gibt Eltern Ideen an die Hand, mit denen sie aus der Schimpffalle heraus- und wieder in den grünen Bereich kommen.
