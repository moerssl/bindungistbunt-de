---
title: So viel Freude, so viel Wut
media_order: nora-imlau-so-viel-freude-so-viel-wut-bindung-ist-bunt.jpg
buch7: https://www.buch7.de/store/product_details/1031403443
amazon: https://amzn.to/2sLHFfv
---
**Wenn Kinder starke Gefühle haben**

Gefühlsstarke Kinder – so nennt Nora Imlau Jungen und Mädchen, die von Geburt an anders sind als andere Kinder: wilder, bedürfnisstärker, fordernder. Aber gleichzeitig auch feinfühliger, sensibler, verletzlicher. Jedes siebte Kind kommt mit dieser besonderen Spielart der Persönlichkeitsentwicklung zur Welt. So viele! Und doch fühlen sich viele Eltern sehr allein, wenn ihr Kind als gefühlt einziges als Baby den Kinderwagen hasst und im Rückbildungskurs nicht auf der Matte liegen mag, auch als Kindergartenkind noch nicht alleine einschlafen kann und selbst im Schulalter noch viel Hilfe im Umgang mit seinen heftigen Gefühlsausbrüchen braucht.

Gewohnt fachkundig und einfühlsam leuchtet Nora Imlau aus, warum gefühlsstarke Kinder sich so von Gleichaltrigen unterscheiden und was sie von ihren Eltern brauchen, um einen gesunden Umgang mit ihren intensiven Emotionen zu erlernen. Plus: Ganz praktische Hilfestellungen für typische Stress- und Konfliktsituationen mit gefühlsstarken Kindern – vom Anziehen über den Kindergarten- und Schulbesuch bis zum Zähneputzen.
