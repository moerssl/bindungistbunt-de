---
title: 'Artgerecht - Das andere Babybuch'
media_order: 'nicola-schmidt-artgerecht-das-andere-baby-buch-cover-bindung-ist-bunt.jpg'
image: 'https://www.randomhouse.de/content/edition/covervoila_hires/Schmidt_Nartgerecht-Baby-Buch_158807.jpg'
buch7: 'https://www.buch7.de/store/product_details/1024610237'
amazon: 'https://amzn.to/2JNriZg'
---

Seit der Steinzeit haben unsere Babys dieselben Bedürfnisse: Nähe, Schutz, Getragensein, essen dürfen, wenn sie hungrig sind, und schlafen dürfen, wenn sie müde sind. Unsere moderne Welt jedoch passt nicht immer zu diesen Bedürfnissen. Wie Eltern dennoch dem biologischen Urprogramm ihrer Kinder gerecht werden können, zeigt dieses Buch: konkret, ermutigend, undogmatisch und nachhaltig.
