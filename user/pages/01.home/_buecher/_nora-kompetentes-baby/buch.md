---
title: Mein kompetentes Baby
media_order: nora-imlau-mein-kompetentes-baby-bindung-ist-bunt.jpg
buch7: https://www.buch7.de/store/product_details/1026718497
amazon: https://amzn.to/2sXjzgK
---
Babys gehören zu den meist unterschätzten Wesen auf diesem Planeten. Dabei ist es absolut erstaunlich, was Babys ab dem ersten Tag schon können: Sie erkennen die Eltern, reagieren auf Gesichter, nehmen Blickkontakt auf, können Stimmungen unterscheiden. Aus Sicht der modernen Entwicklungspsychologie tragen die Kleinen damit aktiv zum Aufbau der Eltern-Kind-Bindung bei.

Nora Imlaus fundierter und leicht lesbarer Ratgeber durch das erste Jahr zeigt anschaulich, dass Babys genau über die Kompetenzen verfügen, die sie in ihrem jeweiligen Lebensalter und in ihrer Erfahrungswelt brauchen. Sie entwickeln sich nicht vom Unfertigen zum Fertigen, sondern werden von kompetenten Neugeborenen zu kompetenten Babys zu kompetenten Kleinkindern. Dieser revolutionäre Blick auf Babys entlastet die Eltern, denn wer versteht, wie Babys „ticken“, erkennt schneller, was sie brauchen, um ausgeglichen und zufrieden zu sein.
