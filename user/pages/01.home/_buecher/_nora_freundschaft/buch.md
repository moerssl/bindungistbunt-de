---
title: 'Freundschaft'
media_order: nora-imlau-freundschaft-bindung-ist-bunt.jpg
buch7: https://www.buch7.de/store/product_details/1022442187
amazon: https://amzn.to/2KPOoz7
---
** Wie Kinder sie erleben und Eltern sie stärken können **

In Freundschaften entdecken Kinder neue Horizonte. Bei Gleichaltrigen lernen sie, auf andere einzugehen und Regeln zu vereinbaren. Jeder Freund ist anders, manchmal sogar unsichtbar. In den Spiel- und Phantasiewelten von Kindern bedeutet »Freunde sein« etwas anderes als für Erwachsene.
