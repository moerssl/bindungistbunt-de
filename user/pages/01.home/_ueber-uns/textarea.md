---
title: Über uns
---
#### Nicola Schmidt

<span class="img-sidebar left">
![Nicola Schmidt von bindung ist bunt](nicola-schmidt-bindung-ist-bunt.jpeg)</span> Nicola Schmidt ist Mitgründerin vom [Artgerecht-Projekt](http://artgerecht-projekt.de), Wissenschaftsjournalistin, Mama von zwei Kindern und Politikwissenschaftlerin. Sie sucht seit der Geburt ihres Sohnes Antworten auf die Frage, was wissenschaftlich belegbar gut für kleine Menschenkinder und für unseren Planeten ist. Es ist ihr wichtig, das Attachment Parenting auf wissenschaftlichen Füßen steht und gleichzeitig ein Ort der Toleranz und der Vielfalt bleibt.


[www.nicolaschmidt.de](http://www.nicolaschmidt.de)
####  Nora Imlau
<span class="img-sidebar right ">
![Nora Imlau von bindung ist bunt](nora-imlau-bindung-ist-bunt.jpeg)</span> Nora Imlau ist Journalistin, Autorin und eine der bekanntesten Fürsprecherinnen bindungsorientierter Elternschaft in Deutschland. Als Mutter von drei Kindern beschäftigt sie sich seit über zehn Jahren jeden Tag nicht nur beruflich, sondern auch privat mit der Frage, wie es gelingen kann, die Bedürfnisse großer und kleiner Menschen im turbulenten Alltag  unter einen Hut zu bekommen und so die Welt zu einem besseren Ort zu machen.

[www.nora-imlau.de](http://www.nora-imlau.de)
