---
title: 'Bindung ist bunt'
menu: Starte hier
body_classes: 'title-center title-h1h2'
routable: true
visible: true
metadata:
  description: Bindung ist bunt! – eine Initiative für sichtbare Solidarität unter Eltern. Bindungsorientiert zu leben, das bedeutet für uns vor allem eins: Wertschätzung. Diese Haltung in die Welt zu tragen, ist uns ein echtes Herzensanliegen.
content:
    items: '@self.modular'
    order:
        by: custom
        custom:
            - _attachment-parenting
            - _secondarynav
            - _ueber-uns
            - _buecher
            - _affiliatehinweis
---
