---
title: FAQ
metadata:
  description: Mit unseren Buttons wollen wir euch die Möglichkeit geben, sichtbar zu werden und einander zu finden – als Eltern, die für bindungsorientiertes Familienleben, Toleranz und Respekt einstehen.
---
# Häufige Fragen
## Warum macht ihr das?
Mit unseren Buttons wollen wir euch die Möglichkeit geben, sichtbar zu werden und einander zu finden – als Eltern, die für bindungsorientiertes Familienleben, Toleranz und Respekt einstehen.

### Kann man euch beitreten?

Wir sind kein Verein, deshalb kann man uns auch nicht beitreten. Ihr könnt Euch einfach einen Button bestellen und so zeigen, dass ihr für Solidarität und Respekt unter Eltern einsteht.

### Wie kann man euch unterstützen?
Bestellt unsere Buttons. Tragt sie als sichtbares Zeichen für mehr Wertschätzung und Solidarität. Und vor allem: Seid nett zueinander.

### Könnt ihr eine Veranstaltung in unserem Ort machen?
Prinzipiell stehen wir für Veranstaltungen gerne zur Verfügung! Schreibt uns einfach eine Nachricht, und wir schauen mit euch gemeinsam, was wir möglich machen können.  

### Kann unser Logo auf diese Website kommen?
Ja, logisch! Unter „Unterstützer“ listen wir gerne Blogs, Vereine und Unternehmen, die diese Initiative unterstützen möchten. Wenn Ihr gerne dabei sein wollt, sprecht uns an unter kontakt@bindungistbunt.de.

### Wie können wir euer Logo/euren Button auf unserer Website einbinden?
Unter „Downloads“ haben wir für euch das Logo, den Button und ein Banner hinterlegt die ihr – mit einer Verlinkung auf die Hauptseite bindungistbunt.de kostenlos auf eure Seite nehmen und so Flagge zeigen könnt.

### Ist dieser Text urheberrechtlich geschützt?
Ja, aber mit Quellenangabe dürft ihr ihn gerne verwenden. Hier zum rauskopieren:
<textarea>
Quelle: Nora Imlau, Nicola Schmidt: <a href="https://Bindungistbunt.de" target="_blank" title="Bindung ist bunt">bindungistbunt.de</a>
</textarea>

### Dürfen wir ihn verwenden?
Ja, natürlich gerne siehe oben.

### Steht ihr für Interviews, Podiumsdiskussionen oder ähnliches zur Verfügung?
Prinzipiell stehen wir für Veranstaltungen gerne zur Verfügung! Schreibt uns einfach eine Nachricht, und wir schauen mit euch gemeinsam, was wir möglich machen können.
