---
title: Download
description: Wir stellen das Logo von Bindung ist bunt zum Download zur Verfügung. Du kannst es nutzen, um es auf deiner Homepage einzubunden.
---
# Download
## Du möchtest die Buttons in deiner eigenen Seite einbinden?
Wir haben etwas Quellcode vorbereitet:

![bindungistbunt](../user/themes/bindungistbunt/images/bindung-ist-bunt-button.jpeg)
<textarea style="font-family: courier; width: 100%" rows="5">
<a href="https://bindungistbunt.de" style="text-decoration: none; border: 0" ><img src="https://bindungistbunt.de/user/themes/bindungistbunt/images/bindung-ist-bunt-button.jpeg" /></a>
</textarea>
